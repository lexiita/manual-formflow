# Visualizando informações e detalhes de um fluxo

`Menu Fluxo > Botão Informações`

A partir da lista de fluxos, é possível visualizar detalhes do registro realizado e inserção de formulários, etapas de tramitação e documentos, bastando clicar no botão **Informações** referente ao item desejado, conforme demonstração:

<figure style="text-align: center;">
  <img  src="../img/fluxo/3_informacoes do fluxo.png" width=""  title="Detalhes do fluxo" />  
  <figcaption style="font-size: 14px; font-style: italic; ">Tela de detalhes do fluxo</figcaption>
</figure>
&nbsp;



1. A tela exibe como destaque eventuais etapas cadastradas e todos os demais campos que foram preenchidos no formulário de cadastro em modo de leitura, para conferência.
2. **Detalhes:** Botão que abre uma nova janela sobreposta, trazendo o conteúdo de descrição do fluxo; 
3. **Formulários:** Botão que lista os formulários cadastrados no fluxo, assim como a ordem em que serão apresentados na solicitação, seu título e se está ativo;
4. **Tramitação Interna:** Botão de acesso a área de gerenciamento da tramitação do fluxo, onde é possivel cadastrar e editar as etapas de tramitação pelo qual o fuxo deverá passar até ser concluído;
5. **Documentos:** Botão de acesso a seção de gerenciamento dos documentos que serão disponibilizados durante o fluxo, tanto documentos de solicitação que ficaram para consulta do órgão como os documentos de resposta que serão entregues para o cidadão;
6. **Formulário:** Botão de acesso a tela de cadastro de novos formulários;
7. **Voltar:** Botão para voltar a lista de fluxos cadastrados.


---

## Inserindo um formulário

`Menu Fluxo > Botão Informações > Botão Formulários > Botão Formulário`

Para atribuir um formulário ao fluxo, a partir da tela de informações, o usuário deverá clicar no botão **Formulário**, a fim de exibir a tela de cadastro: 

<figure style="text-align: center;">
  <img  src="../img/fluxo/4_criando formulario.png" width=""  title="Criando Novo Formulário" />
  <figcaption style="font-size: 14px; font-style: italic; ">Criando Novo Formulário</figcaption>
</figure>
&nbsp;

1. **Ordem**: Campo para inserir o número correspondente à ordem em que o formulário deverá ser preenchido;
2. **Título**: Campo para inserir um título que possa resumir e destacar a etapa;
3. **Descrição**: Área para inserir detalhes do que será preenchido na etapa do formulário;
4. **Está Ativo?**: Caixa que deve ser marcada em caso de formulário vai estar ativo como etapa no fluxo;
5. **Salvar**: Após finalizar o cadastro, o usuário deverá clicar no botão **Salvar** para armazenar os dados;
6. **Cancelar**: Caso não seja possível concluir o registro, o usuário deverá clicar no botão **Cancelar** para retornar à tela de informações do serviço.

Caso seja necessário, o usuário poderá realizar alterações em todas as informações básicas do formulário cadastrado. Para tanto, deverá clicar no botão **Gerenciar** relacionado ao formulário desejado, para que o sistema traga o formulário com os dados abertos para edição.

---

## Construção e edição do formulário

`Menu Fluxo > Botão Informações > Botão Formulários > Botão Construir`

Após cadastrar a descrição do formulário, será possível construir o formulário com os campos a serem preenchidos para recebimento das informações, de acordo com o serviço solicitado pelo fluxo. 
Para tanto, deverá clicar no botão **Construir** relacionado ao formulário desejado, conforme segue exemplo:

<figure style="text-align: center;">
  <img  src="../img/fluxo/5_construindo formulario.png" width=""  title="Construindo um Formulário" />
  <figcaption style="font-size: 14px; font-style: italic; ">Construindo um Formulário</figcaption>
</figure>
&nbsp;

1. **Incluir Campo**: Botão que aciona uma janela sobreposta, contendo formulário de cadastro para inserção de um campo;
2. **Detalhes**: Botão que traz o formulário com os dados preenchidos, disponíveis para edição;
3. **Remover**: Botão que possibilita realizar a exclusão do campo inserido; 
4. **Fechar:** Botão que fecha a janela de construção de formulário;
5. **Escolha o Tipo de Campo:** Selecionar o tipo de informação que o campo vai receber como: texto, número, data, arquivos...;
6. **Ordem:** Campo para inserir o número correspondente à ordem em que o campo deverá ser preenchido;
7. **Título:** Campo para inserir um título para o campo a ser preenchido;
8. **Texto de Ajuda:** Texto informativo sobre os dados a serem inseridos no respectivo campo, ficará localizado abaixo da caixa de texto no formulário;
9. **Máscara:** Permite estabelecer uma formatação do conteúdo, como para CPF, telefone, CEP etc.;
10. **Obrigatório:** Campo para selecionar se o campo será de preenchimento obrigatório;
11. **Texto Interno:** *Placeholder*, texto informativo de ajuda de como o campo deve ser preenchido;
12. **Validador:** Verificador de autenticidade dos dados inseridos;
13. **Cancelar:** Caso não seja possível concluir o registro, o usuário deverá clicar no botão **Cancelar** para retornar à tela de informações do formulário;
14. **Salvar:** Após finalizar o cadastro, o usuário deverá clicar no botão **Salvar** para armazenar os dados.

---

## Lista tramitação interna

`Menu Fluxo > Botão Informações > Botão Tramitação Interna `

Ainda na tela de informações do fluxo, existe também a seção denominada **Tramitação Interna**, na qual é possível acessar ferramentas complementares de cadastro das etapas de tramitação e sua edição:

<figure style="text-align: center;">
  <img  src="../img/fluxo/6_lista de tramitacao.png" width=""  title="Lista de tramitação" />
  <figcaption style="font-size: 14px; font-style: italic; ">Lista de tramitação</figcaption>
</figure>
&nbsp;

1. **Tramitação Interna:** Botão que lista as etapas de tramitação cadastradas do fluxo, assim como a ordem em que serão efetuadas, o seu título e se está ativo;
2. **Tramitação:** Botão de acesso a tela de cadastro de novas etapas de tramitação;
3. **Gerenciar:** Botão de acesso a tela para edição de uma tramitação específica;
4. **Voltar:** Botão para voltar a lista de fluxos cadastrados.

---

## Inserindo uma tramitação interna

`Menu Fluxo > Botão Informações > Botão Tramitação Interna > Botão Tramitação`

Para atribuir uma tramitação ao fluxo, a partir da tela de informações na seção **Tramitação Interna**, o usuário deverá clicar no botão **Tramitação**, a fim de exibir a tela de cadastro: 

<figure style="text-align: center;">
  <img  src="../img/fluxo/7_criando tramitacao.png" width=""  title="Criando Uma Tramitação" />
  <figcaption style="font-size: 14px; font-style: italic; ">Criando Uma Tramitação</figcaption>
</figure>
&nbsp;

1. **Ordem:** Campo para inserir o número correspondente à ordem em que a tramitação deverá ser efetuada;
2. **Atividade:** Campo para inserir um título para a etapa da tramitação;
3. **Órgão:** Caixa de seleção para definir o órgão responsável por efetuar a etapa da tramitação;
4. **Salvar:** Após finalizar o cadastro, o usuário deverá clicar no botão **Salvar** para armazenar os dados;
5. **Cancelar:** Caso não seja possível concluir o registro, o usuário deverá clicar no botão **Cancelar** para retornar à tela de informações do fluxo.

Caso seja necessário, o usuário poderá realizar alterações em todas as informações da tramitação cadastrada. Para tanto, deverá clicar no botão **Gerenciar** relacionado a tramitação desejada, para que o sistema traga o formulário com os dados abertos para edição.

Após finalizar, o usuário deverá clicar no botão **Salvar** para que o sistema armazene as alterações. Caso não tenha certeza das ações realizadas, o usuário deverá clicar no botão **Cancelar** para que o sistema ignore as alterações.

---

## Lista de documentos

`Menu Fluxo > Botão Informações > Botão Documentos `

Ainda na tela de informações do fluxo, existe também a seção denominada **Documentos**, na qual é possível acessar ferramentas complementares de cadastro do fluxo:

<figure style="text-align: center;">
  <img  src="../img/fluxo/8_lista documentos.png" width=""  title="Lista de documentos" />
  <figcaption style="font-size: 14px; font-style: italic; ">Lista de documentos</figcaption>
</figure>
&nbsp;

1. **Documentos:** Botão que lista os documentos associados ao fluxo;
2. **Criar Documento:** Botão de acesso a tela de cadastro de um novo documento;
3. **Gerenciar:** Botão de acesso a tela para edição de um documento específico;
4. **Voltar:** Botão para voltar a lista de fluxos cadastrados.

---

## Criando um documento

`Menu Fluxo > Botão Informações > Botão Documentos > Botão Criar Documento`

Para atribuir um documento ao fluxo, a partir da tela de informações na seção **Documentos**, o usuário deverá clicar no botão **Criar Documento**, a fim de exibir a tela de cadastro: 

<figure style="text-align: center;">
  <img  src="../img/fluxo/9_criando documento.png" width=""  title="Criando um documentos" />
  <figcaption style="font-size: 14px; font-style: italic; ">Criando um documentos</figcaption>
</figure>
&nbsp;

1. **Editar:** Botão de acesso ao formulário de cadastro do documento permitindo assim sua criação e edição;
2. **Prévia:** Botão para ter acesso a uma pré-visualização do documento após finalizado;
3. **Cabeçalho:** Caixa de seleção para definir o cabeçalho que ficará na parte superior do documento;
4. **Tipo de Documento:** Caixa de seleção para definir o tipo do documento a ser emitido, se será de **solicitação** (documento este que fica com o órgão responsável pelo serviço), ou se será um documento de **resposta** (que é o documento final que será entregue ao cidadão);
5. **Assunto:** Campo a ser preenchido com o título do documento;
6. **Conteúdo:** Caixa de texto com ferramentas de formatação, na qual deve ser criado o documento;
7. **Seletores**: Ferramenta que dá acesso às variáveis de coleta de dados geradas a partir dos campos criado nos formulários. Cada variável é listada pelo Nome do Formulário/Nome do Campo e contém o símbolo **$**. Para utilizar uma variável, posicione o cursor do mouse no trecho do documento que deseja inserir a variável, no contexto do documento;
8. **Conteúdo editável após ser salvo:** Caixa de seleção para definir se após a finalização da criação do documento ele poderá ser editado.
 ***Obs:*** esta operação não poderá ser alterada se definido como "não editável";
9. **Assinado automaticamente pelo sistema:** Caixa de seleção onde se define se o serviço será um autoserviço, ou se precisará passar por uma tramitação;

Após concluir o preenchimento de todos os campos obrigatórios, o usuário deverá clicar no botão **Salvar** para que o sistema armazene o cadastro. Caso não seja possível concluir, o usuário deverá clicar no botão **Cancelar** para fechar a janela e ser redirecionado à lista de formulários.

 
Caso seja necessário, o usuário poderá realizar alterações em todas as informações do documento cadastrado. Para tanto, deverá clicar no botão **Gerenciar** relacionado ao documento desejado, para que o sistema traga o formulário com os dados abertos para edição.

Após finalizar, o usuário deverá clicar no botão **Salvar** para que o sistema armazene as alterações. Caso não tenha certeza das ações realizadas, o usuário deverá clicar no botão **Cancelar** para que o sistema ignore as alterações.