# Lista de fluxos cadastrados

`Menu Fluxo`

Clicando no botão **fluxos**, o usuário tem acesso à lista de fluxos cadastrados, podendo realizar pesquisas, edições ou cadastro de novos fluxos:

<figure style="text-align: center;">
  <img  src="../img/fluxo\1_lista de fluxo.png" width=""  title="Lista de fluxos cadastrados" />
  <figcaption style="font-size: 14px; font-style: italic; ">Lista de fluxos cadastrados</figcaption>
</figure>


1. **Buscar**: Botão e caixa para pesquisa rápida de fluxos cadastrados;
2. **Novo Fluxo**: Botão de acesso à tela de cadastro de um novo fluxo;
3. **Gerenciar**: Botão de acesso a área de edição das informações básicas de um fluxo específico;
4. **Informações**: Botão para visualizar mais detalhes de um fluxo e cadastro suas etapas;
5. **Navegação**: Botões de navegação entre as listas de fluxos.