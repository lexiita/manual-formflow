# Gerenciando Fluxos

## Cadastrando um novo fluxo

`Menu Fluxo > Novo Fluxo`

Para criar um fluxo no sistema, o usuário deve clicar no botão **Novo Fluxo**, que irá trazer o formulário ilustrado a seguir:

<figure style="text-align: center;">
  <img  src="../img/fluxo/2_novo fluxo.png" width=""  title="Tela de cadastro de novo fluxo" />
  <figcaption style="font-size: 14px; font-style: italic; ">Tela de cadastro de novo fluxo</figcaption>
</figure>

&nbsp;

1. **Título**: Campo obrigatório para informar o nome do fluxo;
2. **Órgão Responsável**: Campo obrigatório para informar qual órgão oferece o serviço;
3. **Descrição:** Caixa de texto com ferramentas de formatação, na qual deve ser informada obrigatoriamente uma breve descrição do fluxo;
4. **Identificador do serviço:** Nome do serviço ao qual o fluxo vai estar associado;
5. **Ativo**: Caixa que deve ser marcada em caso de fluxo ativo e publicado no Portal Único;
6. **Cancelar:** Caso não seja possível concluir, o usuário deverá clicar no botão **Cancelar** para fechar a janela e ser redirecionado à lista de fluxos.
7. **Salvar:** Após concluir o preenchimento de todos os campos obrigatórios, o usuário deverá clicar no botão **Salvar** para que o sistema armazene o cadastro.

---

## Editando um fluxo cadastrado

`Menu Fluxo > Botão Gerenciar`

Caso seja necessário, o usuário poderá realizar alterações em todas as informações básicas do fluxo cadastrado. Para tanto, deverá clicar no botão **Gerenciar** relacionado ao fluxo desejado, para que o sistema traga o formulário com os dados abertos para edição.

Após finalizar, o usuário deverá clicar no botão **Salvar** para que o sistema armazene as alterações. Caso não tenha certeza das ações realizadas, o usuário deverá clicar no botão **Cancelar** para que o sistema ignore as alterações.