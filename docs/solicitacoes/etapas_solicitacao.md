# Visualizando informações e detalhes de uma solicitação

`Menu Solicitações > Botão Visualizar`

A partir da lista de solicitações, é possível visualizar detalhes da solicitação realizada e identificar o status que em que se encontra, bastando clicar no botão **Visualizar** referente ao item desejado, conforme demonstração:

<figure style="text-align: center;">
  <img  src="../img/solicitacoes/2_etapas tramitacao.png" width=""  title="Tela de detalhes de uma solicitação" />  
  <figcaption style="font-size: 14px; font-style: italic; ">Tela de detalhes de uma solicitação</figcaption>
</figure>
&nbsp;

1. **Informações da Solicitação:** Caixa com os principais dados da solicitação como: *protocolo, hash, identificador e cpf*;
2. **Tramitar:** Botão de acesso a área de tramitação da solitação, como ilustrado na imagem seguinte;
3. **Tramitação:** Caixa de informações onde se pode acompanhar o status da solicitação;
4. **Histórico:** Seção que lista o histórico de tramitação de uma solicitação.

---

## Tramitando uma solicitação

`Menu Solicitações > Botão Visualizar > Botão Tramitar`

Para realizar a tramitação de uma solicitação, a partir da tela de informações, o usuário deverá clicar no botão **Tramitar**, a fim de exibir a tela a seguir:

<figure style="text-align: center;">
  <img  src="../img/solicitacoes/3_tramitacao.png" width=""  title="Tramitando uma solicitação" />  
  <figcaption style="font-size: 14px; font-style: italic; ">Tramitando uma solicitação</figcaption>
</figure>
&nbsp;

1. **Etapa/Órgão:** Campo para selecionar o órgão responsável pela etapa da tramitação. Este campo ja vem pré-definido de acordo com a fase em que se encontra o serviço, não pode ser alterado;
2. **Status:** Caixa que abre uma lista de status para o usuário determinar em qual status a solicitação vai estar;
3. **Resumo:** Caixa de texto para o usuário fazer um breve comentário sobre a etapa vigente, que será recebido ao cidadão a respeito de sua solicitação;

Caso não seja possível concluir, o usuário deverá clicar no botão **Cancelar** para fechar a janela e ser redirecionado à tela de informações da solicitação. Após concluir o preenchimento de todos os campos obrigatórios, o usuário deverá clicar no botão **Salvar** para que o sistema armazene o cadastro. 

Ao clicar no botão **salvar** o sistema abrirá uma janela sobreposta, solicitando um código verificador que será enviado para o e-mail do atendente, para desta forma assinar eletronicamente o documento e finalizar esta etapa de tramitação:

<figure style="text-align: center;">
  <img  src="../img/solicitacoes/4_tramitacao.png" width=""  title="Assinando uma tramitação" />  
  <figcaption style="font-size: 14px; font-style: italic; ">Assinando uma tramitação</figcaption>
</figure>
&nbsp;

***Obs:*** Esta fase de tramitação e assinatura se repete em todas as fases da tramitação de uma solicitação, até que ela seja finalizada e o documento final gerado e enviado para o cidadão. 


## Etapas da Tramitação

`Menu Solicitações > Botão Visualizar > Botão Tramitar`

Ainda na tela de informações da solicitação, existe também a seção denominada **Outras Informações**, na qual é possível acessar as etapas da tramitação de uma solicitação e suas ferramentas complementares, como seu histórico, documentos da solicitação, anexos recebidos e o seu progresso, como apresentados a seguir:

###### 1. Histórico

Seção onde se pode ver todos os processos por onde a solicitação já passou e verificar seu atual status. 

<figure style="text-align: center;">
  <img  src="../img/solicitacoes/5_historico.png" width=""  title="Histórico da tramitação" />  
  <figcaption style="font-size: 14px; font-style: italic; ">Histórico da tramitação</figcaption>
</figure>
&nbsp;

###### 2. Documentos da Solicitação

Seção onde se pode visualizar os documentos já preenchidos com os dados do cidadão requerente, dados estes fornecidos no preenchimento do formulário de solicitação:

<figure style="text-align: center;">
  <img  src="../img/solicitacoes/6_documentos.png" width=""  title="Documentos da Solicitação" />  
  <figcaption style="font-size: 14px; font-style: italic; ">Documentos da Solicitação</figcaption>
</figure>
&nbsp;

1. **Visualizar:** Botão de acesso a área de vizualização do documento de solicitação do cidadão requerente (documentos de solicitação ficam somente atrelados ao processo);
2. **Imprimir:** Botão de atalho para imprimir o documento;
3. **Recusar:** Ação em que o atende recusa o documento de solicitação devido alguma inconsistência;
4. **Aceitar:** Ação em que o atendente valida e aceita o documento de solicitação, desta forma dando andamento na tramitação.

###### 3. Documentos de Resposta

Esta é a seção onde ficam listados os documentos de resposta e que serão entregues ao cidadão solicitante, podendo também ser criado um novo documento como mostrado a seguir:

<figure style="text-align: center;">
  <img  src="../img/solicitacoes/7_resposta1.png" width=""  title="Documentos de Resposta" />  
  <figcaption style="font-size: 14px; font-style: italic; ">Documentos de Resposta</figcaption>
</figure>
&nbsp;

1. **Criar Documento:** Botão de acesso a área de criação de um novo documento de resposta, como ilustrado a imagem seguinte;
2. **Visualizar:** Botão de acesso a uma pré visualização do documento de resposta final que será entregue ao cidadão solicitante;
3. **Imprimir:** Botão de atalho para a impressão do documento;
4. **Assinatura Digital:** Carimbo indicador da assinatura do cidadão eletronicamente validada;
5. **Voltar:** Botão para voltar a tela de informações da solicitação.

<figure style="text-align: center;">
  <img  src="../img/solicitacoes/7_resposta2.png" width=""  title="Criando um novo documento de resposta" />  
  <figcaption style="font-size: 14px; font-style: italic; ">Criando um novo documento de resposta</figcaption>
</figure>
&nbsp;

1. **Editar:** Botão de acesso ao formulário de criação do ducumento;
2. **Prévia:** Botão de acesso à uma pré visualização do documento final;
3. **Assunto:** Campo a ser preenchido com o título do documento;
4. **Cabeçalho:** Caixa de seleção para escolher entre os cabeçalhos já pré-cadastrados pelo órgão responsável;
5. **Conteúdo:** Caixa de texto com ferramentas de formatação, na qual deve ser criado o documento de resposta;
6. **Cancelar:** Caso não seja possível concluir, o usuário deverá clicar no botão **Cancelar** para fechar a janela e ser redirecionado à lista de formulários;
7. **Salvar:** Após concluir o preenchimento de todos os campos obrigatórios, o usuário deverá clicar no botão **Salvar** para que o sistema armazene o cadastro.


###### 4. Anexos Recebidos

Nesta seção ficam listados todos os arquivos que foram enviados pelo cidadão no momento da solicitação, para serem avaliados e validados:

<figure style="text-align: center;">
  <img  src="../img/solicitacoes/8_anexos.png" width=""  title="Lista de Anexos Recebidos" />  
  <figcaption style="font-size: 14px; font-style: italic; ">Lista de Anexos Recebidos</figcaption>
</figure>
&nbsp;

1. **Visualizar:** Botão de acesso ao arquivo para conferência.

###### 5. Progresso

Seção onde fica registrado o progesso e andamento da solicitação até o momento em que seja finalizada.

<figure style="text-align: center;">
  <img  src="../img/solicitacoes/9_progresso.png" width=""  title="Progresso de uma tramitação" />  
  <figcaption style="font-size: 14px; font-style: italic; ">Progresso de uma tramitação</figcaption>
</figure>
&nbsp;