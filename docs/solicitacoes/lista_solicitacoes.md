# Lista de solicitações

` Menu Solicitações`

Clicando no menu **Solicitações**, o usuário tem acesso à lista de solicitações realizadas, separadas pelos status de tramitação para melhor gerenciamento.

<figure style="text-align: center;">
  <img  src="../img/solicitacoes/1_lista de solicitacoes.png" width=""  title="Lista de solicitações pendentes" />
  <figcaption style="font-size: 14px; font-style: italic; ">Lista de solicitações pendentes</figcaption>
</figure>


1. **Buscar** : Botão e caixa para pesquisa rápida de solicitações realizadas, podendo ser filtrada por número de CPF ou protocólo;
2. **Status de Tramitação** : São as fases pelas quais uma tramitação deverá passar até ser finalizada e o documento final seja gerado para o cidadão;
3. **Visualizar** : Botão de acesso para visualizar mais detalhes de uma solicitação e dar andamento na tramitação quando necessário; 
4. **Navegação** : Botões de navegação entre as listas de solicitações.
