# Acessando o Formflow 

 Para acessar o **Formflow**, o usuário deve inserir o link informado pela equipe administrativa em seu navegador de preferência. 
 
 Será exibida a página inicial do sistema com o botão de acesso para ingressar com a conta Gov.br. 

<figure style="text-align: center;">
  <img  src="../img/tela_inicial_de_login.png" width=""  title="Tela de login Formflow" />
  <figcaption style="font-size: 14px; font-style: italic; ">Tela de login do Formflow</figcaption>
</figure>
&nbsp;

Inserindo os dados CPF e senha nos campos apropriados e clicando posteriormente no botão **Entrar**, o usuário poderá autenticar-se no sistema gerenciador:


<figure style="text-align: center;">
  <img  src="../img/tela_login_govbr.png" width=""  title="Tela de login do Serviços" />
  <figcaption style="font-size: 14px; font-style: italic; ">Tela de login da Conta Gov.br</figcaption>
</figure>
&nbsp;


<figure style="text-align: center;">
  <img  src="../img/tela_senha_govbr.png" width=""  title="Tela de login do Serviços" />
  <figcaption style="font-size: 14px; font-style: italic; ">Tela de senha da Conta Gov.br</figcaption>
</figure>
&nbsp;

* Ao final da autenticação, caso apareça a tela exibindo os *Termos de Uso*, basta dar o aceite clicando no botão, confirmar e proseguir ao sistema.