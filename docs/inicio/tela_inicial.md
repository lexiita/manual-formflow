# Acesso à área administrativa do sistema

O usuário, após autenticar-se no sistema, terá acesso às funções descritas abaixo, conforme demonstração:

<figure style="text-align: center;">
  <img  src="../img/tela_inicial.png" width=""  title="Tela inicial - Ambiente Admin" />
  <figcaption style="font-size: 14px; font-style: italic; ">Tela inicial - Ambiente Administrativo do Formflow</figcaption>
</figure>
&nbsp;

1.	**Home:** Botão de retorno à página inicial do sistema;
2.	**Fluxo:** Botão de acesso a ferramentas de criação e gerenciamento dos fluxos de serviços criados;
3.	**Solicitações:** Botão de acesso ao gerenciamento e tramitações das solicitações realizadas pelos cidadãos através do Portal;
4. **Órgãos:** Botão de acesso a lista de órgãos vinculados ao sistema;
5. **Nome:**Clicando no nome do usuário, é aberto o atalho para sair do sistema;
6. Caixa contendo números consolidados de **fluxos ativos** cadastrados;
7. Índice de solicitações que aguardam resposta;
8. Quadro informativo dos **serviços mais solicitados**;