# Lista de órgãos

` Menu Órgãos`

Clicando no menu **Órgãos**, o usuário tem acesso à lista e a área de gerência de órgãos ativos no sistema, podendo realizar pesquisa e edição de dados.

<figure style="text-align: center;">
  <img  src="../img/orgaos/1_lista orgaos.png" width=""  title="Lista de solicitações pendentes" />
  <figcaption style="font-size: 14px; font-style: italic; ">Lista de solicitações pendentes</figcaption>
</figure>


1. **Buscar**: Botão e caixa para pesquisa rápida de órgãos, podendo ser filtrada por nome ou sigla;
2. **Visualizar**: Botão de acesso para visualizar mais detalhes de um órgão; 
3. **Navegação**: Botões de navegação entre as listas de órgãos.
 