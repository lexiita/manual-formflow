# Detalhes do órgão

`Menu Órgãos > Botão Visualizar `

Para acessar mais detalhes do órgão, o usuário deverá clicar no botão **Visualizar** referente ao registro desejado. O sistema irá abrir a tela exemplificada a seguir:

<figure style="text-align: center;">
  <img  src="../img/orgaos/2_informacoes orgaos.png" width=""  title="Tela de detalhes do órgão" />
  <figcaption style="font-size: 14px; font-style: italic; ">Tela de detalhes do órgão</figcaption>
</figure>
&nbsp;

1. **Informações do Órgão**: Seção com informações referentes ao órgão em destaque;
2. **Outras Informações**: Seção que lista usuários e cabeçalhos associados ao órgão;
3. **Usuários**: Seção que lista os usuários associados ao órgão;
4. **Voltar**: Botão que permite voltar a lista de órgãos.

---

## Cabeçalhos de um órgão

`Menu Órgãos > Botão Visualizar > Botão Cabeçalhos `

Ainda na tela de informações do órgão, existe também a seção denominada **Cabeçalhos**, na qual é possível acessar ferramentas complementares de cadastro e edição dos cabeçalhos do órgão selecionado, como ilustrado a seguir:

<figure style="text-align: center;">
  <img  src="../img/orgaos/3_cabecalhos.png" width=""  title="Cabeçalhos de um órgão" />
  <figcaption style="font-size: 14px; font-style: italic; ">Cabeçalhos de um órgão</figcaption>
</figure>
&nbsp;

1. **Cabeçalhos:** Seção de gerenciamento de cabeçalhos de um órgão;
2. **Criar Cabeçalho:** Botão de acesso ao formulário de criação de um novo cabeçalho;
3. **Editar:** Botão que exibe o formulário de edição de dados do cabeçalho selecionado;
4. **Visualizar:** Botão que abre uma janela sobreposta, com uma pré visualização do resultado final do cabeçalho;
5. **Excluir:** Botão para excluir um cabeçalho;
6. **Voltar:** Botão que permite voltar a lista de órgãos cadastrados.


## Criando um cabeçalho

`Menu Órgãos > Botão Visualizar > Botão Cabeçalhos > Botão Criar Cabeçalho `

Para adicionar um cabeçalho, o usuário deverá clicar no botão **Criar Cabeçalho**, na seção de Informações do Órgão:

<figure style="text-align: center;">
  <img  src="../img/orgaos/4_novo cabecalho.png" width=""  title="Criando um cabeçalho" />
  <figcaption style="font-size: 14px; font-style: italic; ">Criando um cabeçalho</figcaption>
</figure>
&nbsp;

1. **Título:** Campo obrigatório para informar o nome do cabeçalho;
2. **Conteúdo:** Caixa de texto com ferramentas de formatação e inserção de imagens, na qual deve ser criado o cabeçalho;
3. **Cancelar:** Caso não seja possível concluir, o usuário deverá clicar no botão **Cancelar** para fechar a janela e ser redirecionado à lista de órgãos;
4. **Salvar:** Após concluir o preenchimento de todos os campos obrigatórios, o usuário deverá clicar no botão **Salvar** para que o sistema armazene o cadastro.
