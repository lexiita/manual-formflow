# Gestão do Sistema Formflow

## O que é

Sistema que possibilita a gerência e criação de fluxos para autoserviços e serviços digitais disponíveis no Portal do Cidadão.

## Meios de acesso

O Gerenciador de Serviços pode ser acessado por:

* Computador
* Laptop/notebook
* Smartphone

## Forma de Autenticação
 
A Conta gov.br é um meio de acesso digital do usuário aos serviços públicos digitais, ela oferece um ambiente de autenticação digital único do usuário aos serviços públicos digitais, ou seja, com um único usuário e senha você poderá utilizar todos os serviços públicos digitais que estejam integrados. 

Essa é a nova proposta do Governo do Estado, para facilitar a identificação e autenticação do cidadão, privilegiando a governança e a convergência autoritativa, e finalmente o controle de acesso unificado.

## Como recuperar senha de acesso

Esta funcionalidade deve ser realizada apenas pelo site [https://acesso.gov.br](https://acesso.gov.br).

## Navegador

Todos os navegadores são compatíveis com o sistema gerenciador de serviços, mas recomenda-se a utilização do Google Chrome. 

----

``Documentação atualizada em 26/10/22.``
